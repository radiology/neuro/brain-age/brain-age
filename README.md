# Brain Age

This project houses code used for gray matter age prediction as a biomarker for risk of dementia.
The original paper can be referenced [here](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6800321/)
